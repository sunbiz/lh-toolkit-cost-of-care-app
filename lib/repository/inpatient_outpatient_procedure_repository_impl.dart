import 'package:cost_of_care/models/outpatient_procedure.dart';
import 'package:dio/dio.dart';

import '../main.dart';
import '../network/gitlab_api_client.dart';

class InpatientOutpatientProcedureRepositoryImpl {
  BaseOptions options = new BaseOptions(
      connectTimeout: 15 * 1000, // 60 seconds
      receiveTimeout: 15 * 1000 // 60 seconds
      );

//Inpatient Caching Scheme
  bool checkSavedInpatient() {
    if (listbox.containsKey('inpatientProcedure')) {
      return true;
    }
    return false;
  }

  List<OutpatientProcedure> getSavedInpatient() {
    List<OutpatientProcedure> inpatient =
        listbox.get('inpatientProcedure').cast<OutpatientProcedure>();
    return inpatient;
  }

  void saveInpatient(List<OutpatientProcedure> inpatientProcedure) {
    listbox.put('inpatientProcedure', inpatientProcedure);
  }

//Outpatient Caching Scheme
  bool checkSavedOutpatient() {
    if (listbox.containsKey('outpatientProcedure')) {
      return true;
    }
    return false;
  }

  List<OutpatientProcedure> getSavedOutpatient() {
    List<OutpatientProcedure> outpatient =
        listbox.get('outpatientProcedure').cast<OutpatientProcedure>();
    return outpatient;
  }

  void saveOutpatient(List<OutpatientProcedure> outpatientProcedure) {
    listbox.put('outpatientProcedure', outpatientProcedure);
  }

  Future getInpatientProcedures() {
    GitLabApiClient gitLabApiClient = GitLabApiClient(Dio(options));
    return gitLabApiClient.getInpatientProcedures();
  }

  Future getOutpatientProcedures() {
    GitLabApiClient gitLabApiClient = GitLabApiClient(Dio(options));
    return gitLabApiClient.getOutpatientProcedures();
  }
}
